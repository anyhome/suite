<?php
return [

  //定时任务
  'crontab' =>[
    'database'=>'crontab',
    'run_name'=>'suite',
    'pid_file'=>'',
    'logs_path'  =>  Env::get('runtime_path'),
    'tasks' =>  [
      // [
      //   'type'  =>  'class',
      //   'name'=>  '台州-打包传输', 
      //   'cron'=>  '01 * * * * *', 
      //   'url'=>  'index/controller/Taizhou/zip',
      //   'args'  =>  '',
      // ],
    ],
  ],

  //文件同步配置
  'sync' =>[
    
    // 'remote'=>'http://admin.tpl.ali2app.com/layadmin',
    'remote'=>'http://tien.ali2app.com/audit',
  ],
  'http' =>[
    'mode'=>'dev',
    'name'=>'suite',
    'daemonize'=>0,
    'app_path' => Env::get('app_path'),

    'pid_file'              => Env::get('runtime_path') . 'suite:http.pid',//swoole主进程pid存放文件
    'log_file'              => Env::get('runtime_path') . 'suite:http.log',//swoole日志存放文件
    'document_root'         => Env::get('root_path') . 'public',//设置静态服务根目录
    'enable_static_handler' => false,//是否由SWOOLE底层自动处理静态文件，TRUE表示SWOOLE判断是否存在静态文件，如果存在则直接返回静态文件信息
    'timer'                 => true,//是否开启系统定时器
    'interval'              => 1000,//系统定时器 时间间隔
    'task_worker_num'       => 2,//swoole 任务工作进程数量
    'user'                  =>'www',//表示swoole worker进程所属的管理员名称，如果要绑定1024以下端口则必须要求具有root权限，如果设置了该项，则除主进程外的所有进程都运行于指定用户下

    'file_monitor'          => false, // 是否开启PHP文件更改监控（调试模式下自动开启）
    'file_monitor_interval' => 100,  // 文件变化监控检测时间间隔（秒）
    'file_monitor_path'     => [], // 文件监控目录 默认监控application和config目录

    'defalut'=>[
      'host' => '0.0.0.0',
      'port' => '9066',
      'pid_file' => '',
    ],
    'dev'=>[
      'host' => '0.0.0.0',
      'port' => '9067',
      'pid_file' => '',
    ],
  ],
  'tcp' =>[
    'mode'=>'dev',
    'defalut'=>[
      'host' => '0.0.0.0',
      'port' => '9966',
      'pid_file' => '',
      'logs' => '',
    ],
    'dev'=>[
      'host' => '0.0.0.0',
      'port' => '9967',
      'pid_file' => '',
      'logs' => '',
    ],
  ],
  'udp' =>[
    'mode'=>'dev',
    'defalut'=>[
      'host' => '0.0.0.0',
      'port' => '9866',
      'pid_file' => '',
      'logs' => '',
    ],
    'dev'=>[
      'host' => '0.0.0.0',
      'port' => '9867',
      'pid_file' => '',
      'logs' => '',
    ],
  ],
  'websocket' =>[
    'mode'=>'dev',
    'defalut'=>[
      'host' => '0.0.0.0',
      'port' => '8800',
      'pid_file' => '',
      'logs' => '',
    ],
    'dev'=>[
      'host' => '0.0.0.0',
      'port' => '8801',
      'pid_file' => '',
      'logs' => '',
    ],
  ]

];
